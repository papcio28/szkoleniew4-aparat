package com.android.szkolenie.szkoleniew4_aparat;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class MainActivity extends ActionBarActivity {

    public static final int CAMERA_REQUEST = 20;
    public static final int GALLERY_REQUEST = 21;
    public static final String EXTRA_PHOTO_PATH = "photoPath";

    @InjectView(R.id.podglad)
    protected ImageView mPodglad;

    // Zmienna na potrzeby tymczasowego przechowywania ścieżki i nazwy pliku robionego zdjęcia.
    private Uri photoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        photoPath = savedInstanceState.getParcelable(EXTRA_PHOTO_PATH);
        // Odtwarzanie zdjecia po przekreceniu ekranu
        if(photoPath != null) {
            Picasso.with(this)
                    .load(photoPath)    // Skad ma zaladowac obrazek
                    .resize(640, 480)   // ZMniejsz do rozmiaru 640x480
                    .centerInside()     // Wyśrodkuj bez przycinania
                    .into(mPodglad);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(EXTRA_PHOTO_PATH, photoPath);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Obsługa przychodzącego zdjęcia z aparatu
        if(requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            // W tym momencie wiemy, że jest to odpowiedz z aparatu
            // i ze zdjecie zostalo wykonane i zatwierdzone
            Picasso.with(this)
                    .load(photoPath)    // Skad ma zaladowac obrazek
                    .resize(640, 480)   // ZMniejsz do rozmiaru 640x480
                    .centerInside()     // Wyśrodkuj bez przycinania
                    .into(mPodglad);    // Załaduj finalny obrazek do wskazanego ImageView
        }

        // Obsluga przychodzacego zdjecia z galerii
        if(requestCode == GALLERY_REQUEST && resultCode == Activity.RESULT_OK) {
            // Pobieramy sciezke do zdjecia zwrocona z galerii
            photoPath = data.getData();

            // Wyswietlamy zwrócone zdjęcie
            Picasso.with(this)
                    .load(photoPath)    // Skad ma zaladowac obrazek
                    .resize(640, 480)   // ZMniejsz do rozmiaru 640x480
                    .centerInside()     // Wyśrodkuj bez przycinania
                    .into(mPodglad);
        }
    }

    public Uri getPhotoFile() {
        return Uri.fromFile(new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                System.currentTimeMillis() + ".jpeg"));
    }

    // Pobieranie zdjęcia z aparatu
    @OnClick(R.id.btn_aparat)
    public void aparat() {
        Intent mIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        photoPath = getPhotoFile(); // Zapamiętujemy ścieżkę i nazwe pliku do momentu wywolania
        // onActivityResult
        mIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoPath);
        startActivityForResult(mIntent, CAMERA_REQUEST);
    }

    // Pobieranie zdjęcia z galerii
    @OnClick(R.id.btn_galeria)
    public void galeria() {
        Intent mIntent = new Intent(Intent.ACTION_GET_CONTENT);
        mIntent.setType("image/*"); // Potrzebujemy tylko obrazka !
        startActivityForResult(mIntent, GALLERY_REQUEST);
    }
}
